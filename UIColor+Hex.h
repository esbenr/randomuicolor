#import <UIKit/UIKit.h>

#define UIColorFromHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface UIColor (Hex)

+ (UIColor *)colorWithHex:(long)col;
+ (UIColor *)colorWithHexString:(NSString *)str;

@end
